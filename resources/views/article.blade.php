@foreach($articles as $article)
    <div class="col">
        <div class="card shadow-sm">
            <img src="/assets/admin/images/uploads/{{ $article->image }}" class="bd-placeholder-img card-img-top"
                 width="100%" height="225">

            <div class="card-body">
                <p class="card-text">{{ $article->name }}</p>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                        <img class="rounded-circle" src="/assets/admin/images/uploads/{{ $article->author->avatar }}"
                             width="30px" height="30px">
                        {{ $article->author->name }}
                    </div>
                    <small class="text-muted"></small>
                </div>
            </div>
        </div>
    </div>
@endforeach
