@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="section">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Posts Table</h5>
                            <a class="btn btn-info" href="/author/add" >Add New Author</a>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">image</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($authors as $author)
                                    <tr>
                                        <td>{{ $author->id }}</td>
                                        <td>{{$author->name}}</td>
                                        <td>
                                            <img class="rounded-circle" src="/assets/admin/images/uploads/{{$author->avatar}}" width="70px" height="70px">
                                        </td>
                                        <td>
                                            <a href="/author/edit/{{ $author->id }}" class="btn btn-primary"><i
                                                    class="fas fa-edit"></i></a>
                                            <a href="/author/delete/{{ $author->id }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                @include('admin.alert')
                                </tbody>
                            </table>
                            <!-- End Default Table Example -->
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
@endsection
