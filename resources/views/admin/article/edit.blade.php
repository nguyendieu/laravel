@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="section">
            <div class="row">
                <div class="col">

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Add Post</h5>

                            <!-- General Form Elements -->
                            <form action="" method="POST" enctype="multipart/form-data">
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name" value="{{$article->name}}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputNumber" class="col-sm-2 col-form-label">Image</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="file" name="image">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputNumber" class="col-sm-2 col-form-label">Image</label>
                                    <div class="col-sm-10">
                                       <img src="/assets/admin/images/uploads/{{$article->image}}" width="200">
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Content</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" style="height: 100px" name="content" id="content">{{ $article->content }}</textarea>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Author</label>
                                    <div class="col-sm-10">
                                        <select class="form-select" aria-label="Default select example" name="author_id">
                                            <option selected="">chose author</option>
                                            @foreach($author as $item)
                                                <option value="{{ $item->id }}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="row mb-3">

                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary">Submit Form</button>
                                        </div>
                                    </div>
                                @csrf
                            </form><!-- End General Form Elements -->

                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
    <script>
        CKEDITOR.replace( 'content' );
    </script>
@endsection
