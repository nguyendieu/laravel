@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="section">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Posts Table</h5>
                            <a class="btn btn-info" href="/article/add">Add New Post</a>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">image</th>
                                    <th scope="col">Author</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($articles as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>

                                        <td>{{$item->name}}</td>

                                        <td>
                                            <img src="/assets/admin/images/uploads/{{$item->image}}" width="100px">
                                        </td>
                                        <td>{{ $item->author->name }}</td>
                                        <td>
                                            <a href="/article/edit/{{ $item->id }}" class="btn btn-primary"><i
                                                    class="fas fa-edit"></i></a>
                                            <a href="/article/delete/{{ $item->id }}" class="btn btn-danger"><i
                                                    class="fas fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <!-- End Default Table Example -->
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
@endsection
