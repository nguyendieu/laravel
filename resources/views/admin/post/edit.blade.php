@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="section">
            <div class="row">
                <div class="col">

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Add Post</h5>

                            <!-- General Form Elements -->
                            <form action="" method="POST" enctype="multipart/form-data">
                                <div class="row mb-3">
                                    <label for="inputText" class="col-sm-2 col-form-label">Name Post</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name" value="{{$post->name}}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputNumber" class="col-sm-2 col-form-label">Image</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="file" name="image">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-sm-10">
                                        <label for="inputNumber" class="col-sm-2 col-form-label"></label>
                                        <img src="/assets/admin/images/uploads/{{ $post->image }}"
                                             style="width: 200px;">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Description</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" style="height: 100px"
                                                  name="description">{{ $post->description }}</textarea>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Content</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" style="height: 100px" name="content"
                                                  id="content">{{$post->content}}</textarea>
                                    </div>
                                </div>
                                <div class="row mb-3">

                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Submit Form</button>
                                    </div>
                                </div>
                                @csrf
                            </form><!-- End General Form Elements -->

                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection
