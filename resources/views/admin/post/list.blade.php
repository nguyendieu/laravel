@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="section">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Posts Table</h5>
                            <a class="btn btn-info" href="/post/add">Add New Post</a>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">image</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($posts as $post)
                                    <tr>
                                        <td>{{ $post->id }}</td>
                                        <td>{{$post->name}}</td>
                                        <td>
                                            <img src="/assets/admin/images/uploads/{{$post->image}}" width="100px">
                                        </td>
                                        <td>
                                            <a href="/post/edit/{{ $post->id }}" class="btn btn-primary"><i
                                                    class="fas fa-edit"></i></a>
                                            <a href="/post/delete/{{ $post->id }}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <!-- End Default Table Example -->
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
@endsection
