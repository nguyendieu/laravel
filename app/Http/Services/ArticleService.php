<?php

namespace App\Http\Services;

use App\Models\Article;
use App\Models\Author;
use App\Models\Post;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class ArticleService
{
    public function getAll()
    {
        return Article::orderby('id', 'asc')->paginate(20);
    }

    public function getAuthor()
    {
        return Author::orderby('id', 'asc')->get();
    }

    public function getRow($id)
    {
        return Article::where('id', $id)->first();
    }

    public function insert($request)
    {
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $ext;
            $file->move('assets/admin/images/uploads/', $fileName);
        }
        try {
            //dd($request->input());
            Article::create([
                'name' => (string)$request->input('name'),
                'author_id' => (string)$request->input('author_id'),
                'content' => (string)$request->input('content'),
                'image' => $fileName
            ]);
            Session::flash('success', 'Add category successfully');
        } catch (\Exception $err) {
            Session::flash('error', $err->getMessage());
            return false;
        }
        return true;
    }

    public function update($id, $request): bool
    {

        $article = Article::where('id', $id)->first();
        if ($request->hasFile('image')) {
            $path = 'assets/admin/images/uploads' . $article->image;
            if (File::exists($path)) {
                File::delete($path);
            }
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $ext;
            $file->move('assets/admin/images/uploads/', $fileName);
            $article->image = $fileName;
        }

        $article->name = (string)$request->input('name');
        $article->content = (string)$request->input('content');
        $article->author_id = (int)$request->input('author_id');
        $article->save();
        Session::flash('success', 'Update category successfully');
        return true;
    }

    public function destroys($id): bool
    {
        $article = Article::where('id', $id)->first();

        if ($article) {
            $article->delete();
            return true;
        }
        return false;
    }
}
