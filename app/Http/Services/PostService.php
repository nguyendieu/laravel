<?php

namespace App\Http\Services;

use App\Models\Post;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class PostService
{
    public function getAll()
    {
        return Post::orderby('id', 'asc')->paginate(20);
    }

    public function create($request): bool
    {
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $ext;
            $file->move('assets/admin/images/uploads/', $fileName);
        }
        try {
            //dd($request->input());
            Post::create([
                'name' => (string)$request->input('name'),
                'description' => (string)$request->input('description'),
                'content' => (string)$request->input('content'),
                'image' => $fileName
            ]);
            Session::flash('success', 'Add category successfully');
        } catch (\Exception $err) {
            Session::flash('error', $err->getMessage());
            return false;
        }
        return true;
    }

    public function show($id)
    {
        return Post::where('id', $id)->first();
    }

    public function update($id, $request): bool
    {
        $post = Post::where('id', $id)->first();
        if ($request->hasFile('image')) {
            $path = 'assets/admin/images/uploade' . $post->image;
            if (File::exists($path)) {
                File::delete($path);
            }
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $ext;
            $file->move('assets/admin/images/uploads/', $fileName);
            $post->image = $fileName;
        }

        $post->name = (string)$request->input('name');
        $post->description = (string)$request->input('description');
        $post->content = (string)$request->input('content');
        $post->save();
        Session::flash('success', 'Update category successfully');
        return true;
    }

    public function destroys($id):bool
    {
        $post = Post::where('id', $id)->first();

        if ($post) {
            $post->delete();
            Session::flash('success', 'Delete successfully');
            return true;
        }
    }
}
