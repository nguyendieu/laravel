<?php

namespace App\Http\Services;

use App\Models\Article;

class HomeService
{
    const LIMIT = 6;

    public function getArticle()
    {
        return Article::orderby('id', 'asc')->paginate(6);
    }

    public function get($page)
    {
        return Article::orderby('id', 'asc')
            ->when($page != null, function ($query) use ($page) {
                $query->offset($page * self::LIMIT);
            })
            ->limit(self::LIMIT)
            ->get();
    }
}
