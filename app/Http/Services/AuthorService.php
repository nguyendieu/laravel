<?php

namespace App\Http\Services;

use App\Models\Article;
use App\Models\Author;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class AuthorService
{
    public function getAll()
    {
        return Author::orderby('id', 'asc')->paginate(10);
    }

    public function insert($request): bool
    {
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $ext = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $ext;
            $file->move('assets/admin/images/uploads/', $fileName);
        }
        try {
            Author::create([
                'name' => (string)$request->input('name'),
                'avatar' => (string)$fileName
            ]);
            Session::flash('success', 'Add author successfully');
        } catch (\Exception $err) {
            Session::flash('error', $err->getMessage());
            return false;
        }
        return true;
    }

    public function show($id)
    {
        return Author::where('id', $id)->first();
    }

    public function update($id, $request):bool
    {
        $author = Author::where('id', $id)->first();

        if ($request->hasFile('avatar')) {
            $path = 'assets/admin/images/uploads' . $author->avatar;
            if (File::exists($path)) {
                File::delete($path);
            }
            $file = $request->file('avatar');
            $ext = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $ext;
            $file->move('assets/admin/images/uploads/', $fileName);
            $author->avatar = $fileName;
        }

        $author->name = (string)$request->input('name');
        $author->save();
        return true;
    }

    public function destroys($id):bool
    {
        $author = Author::where('id', $id)->first();
        $article = Article::where('author_id',$id)->first();

        if ($article){
            Session::flash('error', 'Delete fails ! ');
            return false;
        }
        else{
            $author->delete();
            Session::flash('success', 'Delete successfully');
            return true;
        }
    }
}
