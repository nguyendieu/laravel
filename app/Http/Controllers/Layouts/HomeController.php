<?php

namespace App\Http\Controllers\Layouts;

use App\Http\Controllers\Controller;
use App\Http\Services\HomeService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $homeService;
    public function __construct(HomeService $homeService)
    {
        $this->homeService= $homeService;
    }

    public function index(){
        return view('welcome',[
            'articles'=> $this->homeService->getArticle()
        ]);
    }
    public function loadArticle(Request $request): \Illuminate\Http\JsonResponse
    {
        $page = $request->input('page', 0);

        $result = $this->homeService->getAllArticle($page);

        if ($result){
            $html = view('article',['articles'=>$result])->render();
            return response()->json([
                'html' => $html
            ]);
        }
        return response()->json(['html'=>'']);
    }
}
