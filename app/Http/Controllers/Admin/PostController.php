<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\PostService;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    protected $postServices;

    public function __construct(PostService $postService)
    {
        $this->postServices = $postService;
    }

    public function index()
    {
        return view('admin.post.list',[
            'posts' => $this->postServices->getAll()
        ]);
    }

    public function create()
    {
        return view('admin.post.add',[

        ]);
    }

    public function store(Request $request)
    {
      $result  =  $this->postServices->create($request);
      if ($result){
          return redirect()->route('list_post');
      }
        return redirect()->back();

    }

    public function edit($id){
        return view('admin.post.edit',[
            'post' => $this->postServices->show($id)
        ]);

    }

    public function update($id,Request $request){
       $result =  $this->postServices->update($id,$request);
       if ($result){
           return redirect()->route('list_post');
       }
       return redirect()->back();
    }

    public function destroys($id){
       $result =  $this->postServices->destroys($id);
       if ($result){
           return redirect()->route('list_post');
       }
       redirect()->back();
    }
}
