<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Http\Services\ArticleService;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    protected $articleService;

    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }

    public function index()
    {
        return view('admin.article.list', [
            'articles' => $this->articleService->getAll()
        ]);
    }

    public function create()
    {
        return view('admin.article.add', [
            'author' => $this->articleService->getAuthor()
        ]);
    }

    public function insert(ArticleRequest $request)
    {
        $result = $this->articleService->insert($request);
        if ($result) {
            return redirect()->route('list_article');
        }
        return redirect()->back();

    }

    public function edit($id)
    {
        return view('admin.article.edit', [
            'author' => $this->articleService->getAuthor(),
            'article' => $this->articleService->getRow($id)
        ]);
    }

    public function update($id, ArticleRequest $request)
    {
        $result = $this->articleService->update($id, $request);
        if ($result) {
            return redirect()->route('list_article');
        }
        return redirect()->back();
    }

    public function destroys($id)
    {
        $result = $this->articleService->destroys($id);
        if ($result) {
            return redirect()->route('list_article');
        }
        return redirect()->back();
    }
}
