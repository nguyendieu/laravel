<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthorRequest;
use App\Http\Services\AuthorService;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    protected $authorService;

    public function __construct(AuthorService $authorService)
    {
        $this->authorService = $authorService;
    }

    public function index()
    {
        return view('admin.author.list', [
            'authors' => $this->authorService->getAll()
        ]);
    }

    public function create()
    {
        return view('admin.author.add');
    }

    public function insert(AuthorRequest $request)
    {
        $result = $this->authorService->insert($request);
        if ($result) {
            return redirect()->route('list_author');
        }
        return redirect()->back();
    }

    public function edit($id)
    {
        return view('admin.author.edit', [
            'author' => $this->authorService->show($id)
        ]);
    }

    public function update($id, AuthorRequest $request)
    {
        $result = $this->authorService->update($id, $request);
        if ($result) {
            return redirect()->route('list_author');
        }
        return redirect()->back();
    }
    public function destroys($id){
        $result =  $this->authorService->destroys($id);

        if (!$result){
            return redirect()->route('list_author');
        }
        return redirect()->route('list_author');
    }

}
