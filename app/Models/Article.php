<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;
    protected $fillable=[
      'name','image','author_id','content'
    ];

    public function author(){
        return $this->hasOne(Author::class,'id','author_id');
    }
}
