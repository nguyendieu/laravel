<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/',[\App\Http\Controllers\Layouts\HomeController::class,'index'])->name('home_page');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth','isAdmin'])->group(function (){
    Route::get('/dashboard',function (){
        return view('home');
    });
    // Post
    Route::get('/post',[\App\Http\Controllers\Admin\PostController::class,'index'])->name('list_post');
    Route::get('post/add',[\App\Http\Controllers\Admin\PostController::class,'create']);
    Route::post('post/add',[\App\Http\Controllers\Admin\PostController::class,'store']);
    Route::get('post/edit/{id}',[\App\Http\Controllers\Admin\PostController::class,'edit']);
    Route::post('post/edit/{id}',[\App\Http\Controllers\Admin\PostController::class,'update']);
    Route::get('post/delete/{id}',[\App\Http\Controllers\Admin\PostController::class,'destroys']);

    //Author
    Route::get('/author',[\App\Http\Controllers\Admin\AuthorController::class,'index'])->name('list_author');
    Route::get('/author/add',[\App\Http\Controllers\Admin\AuthorController::class,'create']);
    Route::post('/author/add',[\App\Http\Controllers\Admin\AuthorController::class,'insert']);
    Route::get('/author/edit/{id}',[\App\Http\Controllers\Admin\AuthorController::class,'edit']);
    Route::post('/author/edit/{id}',[\App\Http\Controllers\Admin\AuthorController::class,'update']);
    Route::get('/author/delete/{id}',[\App\Http\Controllers\Admin\AuthorController::class,'destroys']);

    //Article
    Route::get('/article',[\App\Http\Controllers\Admin\ArticleController::class,'index'])->name('list_article');
    Route::get('article/add',[\App\Http\Controllers\Admin\ArticleController::class,'create']);
    Route::post('article/add',[\App\Http\Controllers\Admin\ArticleController::class,'insert']);
    Route::get('/article/edit/{id}',[\App\Http\Controllers\Admin\ArticleController::class,'edit']);
    Route::post('/article/edit/{id}',[\App\Http\Controllers\Admin\ArticleController::class,'update']);
});


Route::post('/services/load-article',[\App\Http\Controllers\Layouts\HomeController::class,'loadArticle']);
