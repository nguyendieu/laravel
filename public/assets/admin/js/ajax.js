$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function loadMore() {
    var page = $('#page').val();
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        data: {page},
        url: 'services/load-article',
        success: function (result) {
            if (result.html != '') {
                $('#loadArticle').append(result.html);
                $('#hiddenButton').css('display','none')
                //$('#page').val(page + 1)
            }else {
                alert('het du lieu')
                // $('#hiddenButton').css('display','none')
            }
        }
    })
}
